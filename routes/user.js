const express = require("express")
const router = express.Router()

router.use(express.json())
router.post("/user", (req, res) => {

    let { name, age, address, wni } = req.body
    console.log(`name: ${name}`)
    console.log(`age: ${age}`)
    console.log(`address: ${address}`)
    console.log(`wni: ${wni}`)

    res.json({
        message: "success"
    })
})

module.exports = router